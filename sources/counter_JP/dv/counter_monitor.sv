class counter_monitor_before extends uvm_monitor;
	`uvm_component_utils(counter_monitor_before)

	uvm_analysis_port#(counter_transaction) mon_ap_before;

	virtual counter_if vif;

	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction: new

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);

		void'(uvm_resource_db#(virtual counter_if)::read_by_name
			(.scope("ifs"), .name("counter_if"), .val(vif)));
		mon_ap_before = new(.name("mon_ap_before"), .parent(this));
	endfunction: build_phase

	task run_phase(uvm_phase phase);
		// integer counter_mon = 0, state = 0;

		// counter_transaction cnt_tx;
		// cnt_tx = counter_transaction::type_id::create
		// 	(.name("cnt_tx"), .contxt(get_full_name()));

		// forever begin
		// 	@(posedge vif.sig_clock)
		// 	begin
		// 		if(vif.sig_en_o==1'b1)
		// 		begin
		// 			state = 3;
		// 		end

		// 		if(state==3)
		// 		begin
		// 			cnt_tx.out = cnt_tx.out << 1;
		// 			cnt_tx.out[0] = vif.sig_out;

		// 			counter_mon = counter_mon + 1;

		// 			if(counter_mon==3)
		// 			begin
		// 				state = 0;
		// 				counter_mon = 0;

		// 				//Send the transaction to the analysis port
		// 				mon_ap_before.write(cnt_tx);
		// 			end
		// 		end
		// 	end
		// end
	endtask: run_phase
endclass: counter_monitor_before

class counter_monitor_after extends uvm_monitor;
	`uvm_component_utils(counter_monitor_after)

	uvm_analysis_port#(counter_transaction) mon_ap_after;

	virtual counter_if vif;

	counter_transaction cnt_tx;

	//For coverage
	counter_transaction cnt_tx_cg;

	//Define coverpoints
	// covergroup counter_cg;
    //   		ina_cp:     coverpoint cnt_tx_cg.ina;
    //   		inb_cp:     coverpoint cnt_tx_cg.inb;
	// 	cross ina_cp, inb_cp;
	// endgroup: counter_cg

	function new(string name, uvm_component parent);
		super.new(name, parent);
		// counter_cg = new;
	endfunction: new

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);

		void'(uvm_resource_db#(virtual counter_if)::read_by_name
			(.scope("ifs"), .name("counter_if"), .val(vif)));
		mon_ap_after= new(.name("mon_ap_after"), .parent(this));
	endfunction: build_phase

	task run_phase(uvm_phase phase);
		// integer counter_mon = 0, state = 0;
		// cnt_tx = counter_transaction::type_id::create
		// 	(.name("cnt_tx"), .contxt(get_full_name()));

		// forever begin
		// 	@(posedge vif.sig_clock)
		// 	begin
		// 		if(vif.sig_en_i==1'b1)
		// 		begin
		// 			state = 1;
		// 			cnt_tx.ina = 2'b00;
		// 			cnt_tx.inb = 2'b00;
		// 			cnt_tx.out = 3'b000;
		// 		end

		// 		if(state==1)
		// 		begin
		// 			cnt_tx.ina = cnt_tx.ina << 1;
		// 			cnt_tx.inb = cnt_tx.inb << 1;

		// 			cnt_tx.ina[0] = vif.sig_ina;
		// 			cnt_tx.inb[0] = vif.sig_inb;

		// 			counter_mon = counter_mon + 1;

		// 			if(counter_mon==2)
		// 			begin
		// 				state = 0;
		// 				counter_mon = 0;

		// 				//Predict the result
		// 				predictor();
		// 				cnt_tx_cg = cnt_tx;

		// 				//Coverage
		// 				counter_cg.sample();

		// 				//Send the transaction to the analysis port
		// 				mon_ap_after.write(cnt_tx);
		// 			end
		// 		end
		// 	end
		// end
	endtask: run_phase

	// virtual function void predictor();
	// 	cnt_tx.out = cnt_tx.ina + cnt_tx.inb;
	// endfunction: predictor
endclass: counter_monitor_after