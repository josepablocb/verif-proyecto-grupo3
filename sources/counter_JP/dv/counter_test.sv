class counter_test extends uvm_test;

	`uvm_component_utils(counter_test)

	counter_env cnt_env;

	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction: new

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		cnt_env = counter_env::type_id::create(.name("cnt_env"), .parent(this));
	endfunction: build_phase

	task run_phase(uvm_phase phase);
        `uvm_info("counter_test", "Starting sim", UVM_LOW);
		// counter_sequence cnt_seq;

		// phase.raise_objection(.obj(this));
		// 	cnt_seq = counter_sequence::type_id::create(.name("cnt_seq"), .contxt(get_full_name()));
		// 	assert(cnt_seq.randomize());
		// 	cnt_seq.start(cnt_env.cnt_agent.cnt_seqr);
		// phase.drop_objection(.obj(this));
	endtask: run_phase

endclass: counter_test
