class counter_driver extends uvm_driver#(counter_transaction);
	`uvm_component_utils(counter_driver)

	virtual counter_if vif;

	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction: new

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);

		void'(uvm_resource_db#(virtual counter_if)::read_by_name
			(.scope("ifs"), .name("counter_if"), .val(vif)));
	endfunction: build_phase

	task run_phase(uvm_phase phase);
		drive();
	endtask: run_phase

	virtual task drive();
		
		
	endtask: drive
endclass: counter_driver