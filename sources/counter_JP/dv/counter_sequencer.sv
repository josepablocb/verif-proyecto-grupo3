class counter_transaction extends uvm_sequence_item;
	rand bit[1:0] ina;
	rand bit[1:0] inb;
	bit[2:0] out;

	function new(string name = "");
		super.new(name);
	endfunction: new

	`uvm_object_utils_begin(counter_transaction)
		`uvm_field_int(ina, UVM_ALL_ON)
		`uvm_field_int(inb, UVM_ALL_ON)
		`uvm_field_int(out, UVM_ALL_ON)
	`uvm_object_utils_end
endclass: counter_transaction

class counter_sequence extends uvm_sequence#(counter_transaction);
	`uvm_object_utils(counter_sequence)

	function new(string name = "");
		super.new(name);
	endfunction: new

	task body();
		// counter_transaction cnt_tx;

		// repeat(15) begin
		// cnt_tx = counter_transaction::type_id::create(.name("cnt_tx"), .contxt(get_full_name()));

		// start_item(cnt_tx);
		// assert(cnt_tx.randomize());
		// //`uvm_info("cnt_sequence", cnt_tx.sprint(), UVM_LOW);
		// finish_item(cnt_tx);
		// end
	endtask: body
endclass: counter_sequence

typedef uvm_sequencer#(counter_transaction) counter_sequencer;
