interface counter_if;
	logic		sig_clock;
	logic		sig_reset;
	logic		sig_en;
	logic		sig_clr;
    logic       [4:0] sig_inc;

	logic		sig_overflow;
	logic		sig_non_zero;
    logic       [4:0] sig_val;
endinterface: counter_if