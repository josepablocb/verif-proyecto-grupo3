class control_monitor_before extends uvm_monitor;
	`uvm_component_utils(control_monitor_before)

	uvm_analysis_port#(control_transaction) mon_ap_before;

	virtual control_if vif;

	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction: new

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		
		void'(uvm_resource_db#(virtual control_if)::read_by_name
			(.scope("ifs"), .name("control_if"), .val(vif)));
		mon_ap_before = new(.name("mon_ap_before"), .parent(this));
		
	endfunction: build_phase

	task run_phase(uvm_phase phase);
		
		integer counter_mon = 0, state = 0, inicio = 0;

		control_transaction ctr_tx;
		ctr_tx = control_transaction::type_id::create
			(.name("ctr_tx"), .contxt(get_full_name()));

		forever begin
			@(negedge vif.clk)
			begin
				ctr_tx.enable = vif.enable ;
				ctr_tx.val = vif.val ;
				ctr_tx.sop = vif.sop ;
				ctr_tx.eop = vif.eop ;
				ctr_tx.cfg_port_enable = vif.cfg_port_enable ;
				ctr_tx.err = vif.err ;
				// Send the transaction to the analysis port
				mon_ap_before.write(ctr_tx);
				`uvm_info("MONITOR_BEFORE", $sformatf("MONITOR_BEFORE READY"), UVM_LOW);
				`uvm_info("monitor before: ", $sformatf("cfg enable: %0d", vif.cfg_port_enable), UVM_LOW);

				`uvm_info("monitor before: ", $sformatf("enable: %0d", vif.enable), UVM_LOW);

				`uvm_info("monitor before: ", $sformatf("err: %0d", vif.err), UVM_LOW);

				`uvm_info("monitor before: ", $sformatf("sop: %0d", vif.sop), UVM_LOW);

				`uvm_info("monitor before: ", $sformatf("eop: %0d", vif.eop), UVM_LOW);

				`uvm_info("monitor before: ", $sformatf("val: %0d", vif.val), UVM_LOW);
			end
		end
		
	endtask: run_phase
endclass: control_monitor_before

class control_monitor_after extends uvm_monitor;
	`uvm_component_utils(control_monitor_after)

	uvm_analysis_port#(control_transaction) mon_ap_after;

	virtual control_if vif;

	control_transaction ctr_tx;

	//For coverage
	control_transaction ctr_tx_cg;

	
	//Define coverpoints
	// covergroup control_cg;
    //   		val_cp:     coverpoint ctr_tx_cg.val;
    //   		sop_cp:     coverpoint ctr_tx_cg.sop;
	// 		eop_cp:     coverpoint ctr_tx_cg.eop;
	// 		cfg_port_enable_cp: coverpoint ctr_tx_cg.cfg_port_enable;
	// 	cross val_cp, sop_cp, eop_cp, cfg_port_enable_cp;
	// endgroup: control_cg
	

	function new(string name, uvm_component parent);
		super.new(name, parent);
		
		// control_cg = new;
		
	endfunction: new

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		
		void'(uvm_resource_db#(virtual control_if)::read_by_name
			(.scope("ifs"), .name("control_if"), .val(vif)));
		mon_ap_after= new(.name("mon_ap_after"), .parent(this));
		
	endfunction: build_phase

	task run_phase(uvm_phase phase);
		
		integer counter_mon = 0, state = 0, inicio = 0, waiting_eop = 0, sop_missed = 0;
		ctr_tx = control_transaction::type_id::create
			(.name("ctr_tx"), .contxt(get_full_name()));

		forever begin
			@(negedge vif.clk)
			if (!vif.reset_L)
			begin
				ctr_tx.enable = 0;
				ctr_tx.err = 0;
				waiting_eop = 0;
			end
			else begin
				if (waiting_eop) //Ya llego un sop valido
				begin
					//Hay un sop valido y e
					if (vif.sop && vif.val )
					begin
						`uvm_info("MONITOR after", $sformatf("Esperando EOP, y hay un SOP valido"), UVM_LOW);
						ctr_tx.err = 1 ;
						waiting_eop = 0;
						if (sop_missed) begin
							ctr_tx.enable = 0;
						end else begin
							ctr_tx.enable = 1;
						end
						sop_missed = 0;
					end
					else if ( vif.eop && vif.val ) //hay un eop valido
					begin
						`uvm_info("MONITOR after", $sformatf("Esperando EOP, y hay un EOP valido"), UVM_LOW);
						waiting_eop = 0;
						ctr_tx.err = 0;
						if (sop_missed) begin
							ctr_tx.enable = 0;
						end else begin
							ctr_tx.enable = 1;
						end
						sop_missed = 0;
					end
					else begin
						if (sop_missed) begin
							`uvm_info("MONITOR after", $sformatf("Esperando EOP, con un SOP perdido"), UVM_LOW);
							ctr_tx.enable = 0;
							ctr_tx.err = 0;
						end
						else begin
							`uvm_info("MONITOR after", $sformatf("Esperando EOP, y no llega nada valido"), UVM_LOW);
							ctr_tx.enable = 1;
							ctr_tx.err = 0;
							if (vif.enable)
							begin
								waiting_eop  = 1;
							end
							else begin
								waiting_eop  = 0;
							end
						end
					end
				end
				else if (vif.eop && vif.val && vif.sop) //llega un eop y sop valido en el mismo momento
				begin
					`uvm_info("MONITOR after", $sformatf("No esta esperando EOP, y hay un SOP y EOP valido"), UVM_LOW);
					waiting_eop = 0;
					ctr_tx.enable = vif.cfg_port_enable;
					ctr_tx.err =0;
				end
				else if (vif.sop && vif.val) //Lega un SOP valido
				begin
					ctr_tx.enable = vif.cfg_port_enable;
					waiting_eop = 1;
					ctr_tx.err = 0;
					if(!vif.cfg_port_enable) begin  // Se pierde el paquete porque el cfg empezo en 0
						sop_missed = 1;
						`uvm_info("MONITOR after", $sformatf("No esta esperando EOP, y hay un SOP pero se perdera porque cfg esta en 0"), UVM_LOW);
					end
					else begin
						`uvm_info("MONITOR after", $sformatf("No esta esperando EOP, y llega solo un SOP valido"), UVM_LOW);
					end
				end
				else if (vif.eop && vif.val) //Llega un eop valido pero no ha llegado ningun sop
				begin
					`uvm_info("MONITOR after", $sformatf("No esta esperando EOP, y hay un EOP pero no ha llegao el SOP"), UVM_LOW);
					ctr_tx.enable = vif.cfg_port_enable;
					waiting_eop = 0;
					ctr_tx.err = 1;
				end
				else begin
					//no llego ningun sop o eop valido
					`uvm_info("MONITOR after", $sformatf("No esta esperando EOP, y no llega nada valido."), UVM_LOW);
					ctr_tx.enable = vif.cfg_port_enable ;
					ctr_tx.err = 0;
					waiting_eop = 0;
				end
			end
			//Send the transaction to the analysis port
							mon_ap_after.write(ctr_tx);
							`uvm_info("MONITOR_AFTER", $sformatf("MONITOR_AFTER READY"), UVM_LOW);

		end
		
	endtask: run_phase

	
endclass: control_monitor_after