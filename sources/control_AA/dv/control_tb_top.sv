`include "control_pkg.svh"
`include "control_if.sv"
// `include "control_original.v"
// `include "control_arreglado.v"
`include "control.v"

module control_tb_top;
	import uvm_pkg::*;
	import control_pkg::*;

	//Interface declaration
	control_if vif();
	
	//Connects the Interface to the DUT
	control_fsm dut(vif.clk,
			    vif.reset_L,
			    vif.cfg_port_enable,
			    vif.val,
			    vif.sop,
			    vif.eop,
				vif.err,
				vif.enable);

	initial begin
		//Registers the Interface in the configuration block so that other
		//blocks can use it
		uvm_resource_db#(virtual control_if)::set
			(.scope("ifs"), .name("control_if"), .val(vif));

		//Executes the test
		run_test();
	end
	
	//Variable initialization
	initial begin
		vif.clk <= 1'b1;
		vif.reset_L <= 1'b0;

		#20 vif.reset_L <= 1'b1;
	end

	//Clock generation
	always
		#5 vif.clk = ~vif.clk;

	//Dump variables
	initial begin
		$dumpfile("control.vcd");
		$dumpvars(0, control_fsm, control_tb_top);
	end

endmodule
