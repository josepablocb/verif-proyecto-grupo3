class control_transaction extends uvm_sequence_item;
	rand bit [1:0] val;
	rand bit sop;
	rand bit eop;
	rand bit cfg_port_enable;
	bit err;
	bit enable;

	constraint val_range { val > 0; }; // Hacer más probable que val sea 1 que 0

	function new(string name = "");
		super.new(name);
	endfunction: new

	
	`uvm_object_utils_begin(control_transaction)
		`uvm_field_int(val, UVM_ALL_ON)
		`uvm_field_int(sop, UVM_ALL_ON)
		`uvm_field_int(eop, UVM_ALL_ON)
		`uvm_field_int(cfg_port_enable, UVM_ALL_ON)
		`uvm_field_int(err, UVM_ALL_ON)
		`uvm_field_int(enable, UVM_ALL_ON)
	`uvm_object_utils_end
	
endclass: control_transaction

class control_sequence extends uvm_sequence#(control_transaction);
	`uvm_object_utils(control_sequence)

	function new(string name = "");
		super.new(name);
	endfunction: new

	task body();

		integer transac = 0;
		
		control_transaction ctr_tx;

		repeat(15) begin
		ctr_tx = control_transaction::type_id::create(.name("ctr_tx"), .contxt(get_full_name()));
		start_item(ctr_tx);

		case(transac)

			0: begin  // En la transac==0 se prueba que se respete el reset
				assert(ctr_tx.randomize() with { val == 1'b1; sop == 1'b1; eop == 1'b0; cfg_port_enable == 1'b1;});
			end

			1: begin  // Demostracion de error en que enable no se mantiene
				assert(ctr_tx.randomize() with { val == 1'b1; sop == 1'b1; eop == 1'b0; cfg_port_enable == 1'b1;});
			end

			2: begin  // Termina demostracion
				assert(ctr_tx.randomize() with { val == 1'b1; sop == 1'b0; eop == 1'b1; cfg_port_enable == 1'b0;});
			end

			3: begin  // Demostracion de que la señal de error llega atrasada
				assert(ctr_tx.randomize() with { val == 1'b1; sop == 1'b1; eop == 1'b0; cfg_port_enable == 1'b0;});
			end

			4: begin  // Aqui se genera el error, cierra demostracion
				assert(ctr_tx.randomize() with { val == 1'b1; sop == 1'b1; eop == 1'b1; cfg_port_enable == 1'b1;});
			end

			// Demostracion de que enable se mantiene en bajo si el cfg_port_enable inicia con su valor en bajo
			5: begin
				assert(ctr_tx.randomize() with { val == 1'b1; sop == 1'b1; eop == 1'b0; cfg_port_enable == 1'b0;});
			end
			
			6: begin  // Termina demostración
				assert(ctr_tx.randomize() with { val == 1'b1; sop == 1'b0; eop == 1'b1; cfg_port_enable == 1'b1;});
			end
			
			// Lo que siguen son transacciones aleatorias
			default: begin
				assert(ctr_tx.randomize());
			end

		endcase
		transac = transac + 1;
		finish_item(ctr_tx);
		end
		
	endtask: body
endclass: control_sequence

typedef uvm_sequencer#(control_transaction) control_sequencer;
