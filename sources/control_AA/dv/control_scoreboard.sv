`uvm_analysis_imp_decl(_before)
`uvm_analysis_imp_decl(_after)

class control_scoreboard extends uvm_scoreboard;
	`uvm_component_utils(control_scoreboard)

	uvm_analysis_export #(control_transaction) sb_export_before;
	uvm_analysis_export #(control_transaction) sb_export_after;

	uvm_tlm_analysis_fifo #(control_transaction) before_fifo;
	uvm_tlm_analysis_fifo #(control_transaction) after_fifo;

	control_transaction transaction_before;
	control_transaction transaction_after;

	function new(string name, uvm_component parent);
		super.new(name, parent);
		
		transaction_before	= new("transaction_before");
		transaction_after	= new("transaction_after");
		
	endfunction: new

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		
		sb_export_before	= new("sb_export_before", this);
		sb_export_after		= new("sb_export_after", this);

   		before_fifo		= new("before_fifo", this);
		after_fifo		= new("after_fifo", this);
		
	endfunction: build_phase

	function void connect_phase(uvm_phase phase);
		
		sb_export_before.connect(before_fifo.analysis_export);
		sb_export_after.connect(after_fifo.analysis_export);
		
	endfunction: connect_phase

	task run();
		forever begin
			
			before_fifo.get(transaction_before);
			// `uvm_info("Transaction before", transaction_before.sprint(), UVM_LOW);
			after_fifo.get(transaction_after);
			// `uvm_info("Transactin after", transaction_after.sprint(), UVM_LOW);
			
			compare();
			
		end
	endtask: run

	virtual function void compare();

		`uvm_info("transaction before: ", $sformatf("err: %0d", transaction_before.err), UVM_LOW);
		`uvm_info("transaction before: ", $sformatf("enable : %0d", transaction_before.enable), UVM_LOW);

		`uvm_info("transaction after: ", $sformatf("err: %0d", transaction_after.err), UVM_LOW);
		`uvm_info("transaction after: ", $sformatf("enable : %0d", transaction_after.enable), UVM_LOW);
		
		if(transaction_before.enable == transaction_after.enable && transaction_before.err == transaction_after.err) begin
			`uvm_info("compare", {"Test: OK!"}, UVM_LOW);

		end else begin
			`uvm_info("compare", {"Test: Error! Test failed :("}, UVM_LOW);
		end
		
	endfunction: compare
endclass: control_scoreboard