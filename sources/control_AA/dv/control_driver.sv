class control_driver extends uvm_driver#(control_transaction);
	`uvm_component_utils(control_driver)

	virtual control_if vif;

	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction: new

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		
		void'(uvm_resource_db#(virtual control_if)::read_by_name
			(.scope("ifs"), .name("control_if"), .val(vif)));
		
	endfunction: build_phase

	task run_phase(uvm_phase phase);
		
		drive();
		
	endtask: run_phase

	virtual task drive();
		
		control_transaction ctr_tx;
		integer counter = 0, state = 0, transac = 0;
		vif.val = 0'b0;
		vif.sop = 0'b0;
		vif.eop = 0'b0;
		vif.cfg_port_enable = 1'b0;

		forever begin
			if(counter==0)
			begin
				seq_item_port.get_next_item(ctr_tx);
				`uvm_info("ctr_driver", ctr_tx.sprint(), UVM_LOW);
			end
			@(posedge vif.clk)
			`uvm_info("ctr_driver", $sformatf("ctr_driver, counter: %0d", counter), UVM_LOW);
			`uvm_info("ctr_driver", $sformatf("ctr_driver, state: %0d", state), UVM_LOW);
			begin
				if(counter==0)
				begin
					state = 1;
				end

				case(state)
					// El state 1 corresponde a lo que ocurre en el
					// primer ciclo de cada transaccion
					1: begin
						vif.val = ctr_tx.val[0];
						vif.sop = ctr_tx.sop;
						vif.eop = ctr_tx.eop;
						vif.cfg_port_enable = ctr_tx.cfg_port_enable;

						counter = counter + 1;
						if(counter==1) state = 2;
					end

					// El state 2 corresponde a lo que ocurre en los
					// siguientes ciclos de cada transaccion, algunas terminan
					// en los siguientes 7 ciclos y las demás en 3 ciclos
					2: begin
						vif.val = 1'b0;
						vif.sop = 1'b0;
						vif.eop = 1'b0;
						vif.cfg_port_enable = 1'b0;
						counter = counter + 1;

						case(transac)
							1: begin
								if(counter==8) begin
									counter = 0;
									state = 0;
									seq_item_port.item_done();
									transac = transac + 1;
								end
							end

							2: begin
								if(counter==4) begin
									vif.cfg_port_enable = 1'b1;
									counter = 0;
									state = 0;
									seq_item_port.item_done();
									transac = transac + 1;
								end
							end

							3: begin
								if(counter==8) begin
									counter = 0;
									state = 0;
									seq_item_port.item_done();
									transac = transac + 1;
								end
							end

							// Aqui se sube el cfg_port_enable para probar que el 
							// enable no sube con él
							5: begin
								vif.cfg_port_enable = 1'b1;
								if(counter==4) begin
									counter = 0;
									state = 0;
									seq_item_port.item_done();
									transac = transac + 1;
								end
							end

							default: begin
								vif.val = 1'b1;
								if(counter==4) begin
									counter = 0;
									state = 0;
									seq_item_port.item_done();
									transac = transac + 1;
								end
							end
						endcase

					end
				endcase
			end
		end
		
	endtask: drive
endclass: control_driver
