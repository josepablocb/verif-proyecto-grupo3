package control_pkg;

	import uvm_pkg::*;
	`include "uvm_macros.svh"

	`include "control_tlm.sv"
	`include "control_sequence.sv"
	`include "control_sequencer.sv"
	`include "unpacker_sequencer.sv"
	`include "regs_sequencer.sv"
	`include "control_monitor.sv"
	`include "control_driver.sv"
	`include "unpacker_driver.sv"
	`include "regs_driver.sv"
	`include "control_agent.sv"
	`include "c3po_agent.sv"
	`include "control_scoreboard.sv"
	`include "c3po_env.sv"
	`include "control_test.sv"
	`include "unpacker_test.sv"
	`include "regs_test.sv"

endpackage: control_pkg
