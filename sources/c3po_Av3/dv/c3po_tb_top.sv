`include "c3po_pkg.svh"
`include "c3po_if.sv"
`include "control_if.sv"

`timescale 1ns/1ps

module tb();
    import uvm_pkg::*;
	import c3po_pkg::*;

    c3po_if vif_chip();

    localparam WIDTH_P = 4;

    c3po dut (  vif_chip.clk,
                vif_chip.sig_reset_L,

                // CONTROL
			    vif_chip.val,
			    vif_chip.sop,
			    vif_chip.eop,
                
                // UNPACKER
                vif_chip.id,
                vif_chip.sig_vbc,
                vif_chip.sig_data,
                vif_chip.sig_o_val,
                vif_chip.sig_o_sop,
                vif_chip.sig_o_eop,
                vif_chip.sig_o_vbc,
                vif_chip.sig_o_data

                // REGS
                vif_chip.sig_addr,
                vif_chip.sig_rd_wr,
                vif_chip.sig_req,
                vif_chip.sig_cfg_ctrl_err,
                vif_chip.sig_cfg_ctrl_idle,
                vif_chip.sig_cfg_port_enable,
                vif_chip.sig_cfg_port_id,
                vif_chip.sig_write_val,
                vif_chip.sig_read_val,
                vif_chip.sig_ack);

    logic clk;
    logic reset_L;

    initial begin
		//Registers the Interface in the configuration block so that other
		//blocks can use it
		uvm_resource_db#(virtual c3po_if)::set
			(.scope("ifs_chip"), .name("c3po_if"), .val(vif_chip));

		//Executes the test
		run_test();
	end

    //Variable initialization
	initial begin
		vif_chip.clk <= 1'b1;
	end

	//Clock generation
	always
		#5 vif_chip.clk = ~vif_chip.clk;

	//Dump variables
	initial begin
		$dumpfile("c3po.vcd");
		$dumpvars(0, control_fsm, tb);
	end


endmodule
