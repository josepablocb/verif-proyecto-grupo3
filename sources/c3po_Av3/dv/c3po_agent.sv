class c3po_agent extends uvm_agent;
	`uvm_component_utils(c3po_agent)

	// Sequencers
	control_sequencer		ctr_seqr;
	unpacker_sequencer		unp_seqr;
	regs_sequencer			regs_seqr;

	// Drivers
	control_driver			ctr_drvr;
	unpacker_driver			unp_drvr;
	regs_driver				regs_drvr;
	

	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction: new

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);

		ctr_seqr	= control_sequencer::type_id::create(.name("ctr_seqr"), .parent(this));
		unp_seqr	= unpacker_sequencer::type_id::create(.name("unp_seqr"), .parent(this));
		regs_seqr	= regs_sequencer::type_id::create(.name("regs_seqr"), .parent(this));

		ctr_drvr	= control_driver::type_id::create(.name("ctr_drvr"), .parent(this));
		unp_drvr	= unpacker_driver::type_id::create(.name("unp_drvr"), .parent(this));
		regs_drvr	= regs_driver::type_id::create(.name("regs_drvr"), .parent(this));

	endfunction: build_phase

	function void connect_phase(uvm_phase phase);
		super.connect_phase(phase);
		
		ctr_drvr.seq_item_port.connect(ctr_seqr.seq_item_export);
		unp_drvr.seq_item_port.connect(unp_seqr.seq_item_export);
		regs_drvr.seq_item_port.connect(regs_seqr.seq_item_export);
		
	endfunction: connect_phase
endclass: c3po_agent
