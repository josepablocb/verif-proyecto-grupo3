interface c3po_if;

	logic clk;
    logic reset_L;

    logic val,
    logic sop,
    logic eop,
    logic [3:0] id,
    logic [7:0] vbc,
    logic [160*8-1:0] data,

    logic [PORTS_P-1:0] o_val,
    logic [PORTS_P-1:0] o_sop,
    logic [PORTS_P-1:0] o_eop,
    logic [PORTS_P-1:0] [7:0] o_vbc,
    logic [PORTS_P-1:0] [32*8-1:0] o_data,

    logic	[ADDR_SIZE_P-1:0] sig_addr;
	logic	sig_rd_wr;
	logic	sig_req;
	logic   [1:0] sig_cfg_ctrl_err;
    logic   [1:0] sig_cfg_ctrl_idle;
	logic   [1:0] sig_cfg_port_enable;
	logic 	[3:0] [1:0] sig_cfg_port_id;
	logic 	[31:0] sig_write_val;
	logic	[31:0] sig_read_val;
	logic   sig_ack;

    logic [PORTS_P-1:0] [CNT_SIZE_P-1:0] cnt0_val, cnt1_val,

    logic [PORTS_P-1:0] ready

endinterface: control_if
